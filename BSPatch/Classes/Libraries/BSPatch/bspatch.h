//
//  bspatch.h
//  BSPatch
//
//  Created by Abhishek Kumar Singh on 22/01/19.
//  Copyright © 2019 Abhishek Kumar Singh. All rights reserved.
//

#ifndef bspatch_h
#define bspatch_h

static off_t offtin(u_char *buf);
int bspatch_main(int argc,char * ofpath, char * nfpath, char * pfpath);
int bspatch (const char *oldApkPath, const char *newApkPath, const char *patchPath);

#endif /* bspatch_h */
