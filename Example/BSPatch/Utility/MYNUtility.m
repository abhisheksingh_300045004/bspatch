//
//  MYNUtility.m
//  BSPatch_Example
//
//  Created by Abhishek Kumar Singh on 24/01/19.
//  Copyright © 2019 Abhishek Kumar Singh. All rights reserved.
//

#import "MYNUtility.h"
#import "NSString+Addition.h"
#import "BSPatch-umbrella.h"

static NSString * const CurrentMD5   = @"8f8a4422c6cc11b9ddea183b5d1390fb";
static NSString * const ExpectedMD5  = @"e207f77d9de5ec09f580dc51ddce6948";

@implementation MYNUtility

+ (NSURL *)patchFileURL {
    return [NSURL URLWithString:[[NSBundle mainBundle] pathForResource:@"patchfile" ofType:@"binary"]];
}

+ (NSString *)bundlePath {
    return [[NSBundle mainBundle] pathForResource:@"old" ofType:@"binary"];
}

+ (NSURL *)temporaryBundleURL {
    return [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject] URLByAppendingPathComponent:@"temp.bundle"];
}

+ (BOOL)patchFile {
    const char *existingBundlePath = [[self bundlePath] UTF8String];
    const char *generatedBundlePath = [[[self temporaryBundleURL] path] UTF8String];
    const char *patchFilePath = [[[self patchFileURL] path] UTF8String];
    
    int result = bspatch(existingBundlePath, generatedBundlePath, patchFilePath);
    NSString *md5 = [[[NSString alloc] initWithData:[NSData dataWithContentsOfFile:[[self temporaryBundleURL] path]] encoding:NSUTF8StringEncoding] md5];
    
    // Delete Temp Bundle
    if ([[NSFileManager defaultManager] fileExistsAtPath:[[self temporaryBundleURL] path]]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:[[self temporaryBundleURL] path] error:&error];
    }
    return result == 0 && [md5 isEqualToString:ExpectedMD5];
}

@end
