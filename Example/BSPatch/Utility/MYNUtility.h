//
//  MYNUtility.h
//  BSPatch_Example
//
//  Created by Abhishek Kumar Singh on 24/01/19.
//  Copyright © 2019 Abhishek Kumar Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MYNUtility : NSObject
+ (BOOL)patchFile;
@end

NS_ASSUME_NONNULL_END
