//
//  MYNViewController.m
//  BSPatch
//
//  Created by Abhishek Kumar Singh on 01/22/2019.
//  Copyright (c) 2019 Abhishek Kumar Singh. All rights reserved.
//

#import "MYNViewController.h"
#import "MYNUtility.h"

static NSString * const PatchSuccessStr  = @"Patch Successful!";
static NSString * const PatchFailedStr  = @"Patch Failed!";
static NSString * const DefaultStr  = @"Click on Test Patch To Test";

typedef NS_ENUM(NSUInteger, PatchLabelState) {
    Failed,
    Success,
    Default
};

@interface MYNViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblPatchResult;
@end

@implementation MYNViewController

- (void)viewDidLoad{
    [super viewDidLoad];
}

- (void)testPatch {
    [self lblPatchResult].textColor = [UIColor colorWithRed:67.0f/255.0f green:178.0f/255.0f blue:163.0f/255.0f alpha:1.0];
    [self lblPatchResult].text = @"Patching......";
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        PatchLabelState state = [MYNUtility patchFile] ? Success : Failed;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self animatePatchLabelWithState:state];
        });
    });
}

- (void)animatePatchLabelWithState:(PatchLabelState)state {
    NSString *text = nil;
    switch (state) {
            case Default:
            text = DefaultStr;
            break;
            
            case Success:
            text = PatchSuccessStr;
            break;
            
            case Failed:
            text = PatchFailedStr;
            break;
    }
    
    __weak MYNViewController *__weakSelf = self;
    [UIView transitionWithView:[self lblPatchResult] duration:2.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [__weakSelf updatePatchLabelWithSuccess:state != Failed text:text];
    } completion:^(BOOL finished) {
        if (state != Default) {
            [__weakSelf animatePatchLabelWithState:Default];
        }
    }];
}

- (void)updatePatchLabelWithSuccess:(BOOL)success text:(NSString *)text {
    UIColor *color = success ? [[self class] defaultLPatchLabelColor] : [[self class] failedPatchLabelColor];
    [[self lblPatchResult] setTextColor:color];
    [[self lblPatchResult] setText:text];
}

+ (UIColor *)defaultLPatchLabelColor {
    return [UIColor colorWithRed:95.0f/255.0f green:134.0f/255.0f blue:38.0f/255.0f alpha:1.0];
}

+ (UIColor *)failedPatchLabelColor {
    return [UIColor redColor];
}

- (IBAction)btnTestPatchClicked:(id)sender {
    [self testPatch];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end
