//
//  NSString+Addition.m
//  BSPatch_Example
//
//  Created by Abhishek Kumar Singh on 22/01/19.
//  Copyright © 2019 Abhishek Kumar Singh. All rights reserved.
//

#import "NSString+Addition.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString(Addition)

- (NSString *)md5 {
    if (!self) {
        return nil;
    }
    if ([self length] == 0) {
        return nil;
    }
    
    const char *cStr = [self UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest );
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
    [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}

@end
