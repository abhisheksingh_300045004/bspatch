//
//  main.m
//  BSPatch
//
//  Created by Abhishek Kumar Singh on 01/22/2019.
//  Copyright (c) 2019 Abhishek Kumar Singh. All rights reserved.
//

@import UIKit;
#import "MYNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MYNAppDelegate class]));
    }
}
