//
//  MYNAppDelegate.h
//  BSPatch
//
//  Created by Abhishek Kumar Singh on 01/22/2019.
//  Copyright (c) 2019 Abhishek Kumar Singh. All rights reserved.
//

@import UIKit;

@interface MYNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
